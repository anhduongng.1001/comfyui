import os
import random
import sys
from typing import Sequence, Mapping, Any, Union
import torch


def get_value_at_index(obj: Union[Sequence, Mapping], index: int) -> Any:
    """Returns the value at the given index of a sequence or mapping.

    If the object is a sequence (like list or string), returns the value at the given index.
    If the object is a mapping (like a dictionary), returns the value at the index-th key.

    Some return a dictionary, in these cases, we look for the "results" key

    Args:
        obj (Union[Sequence, Mapping]): The object to retrieve the value from.
        index (int): The index of the value to retrieve.

    Returns:
        Any: The value at the given index.

    Raises:
        IndexError: If the index is out of bounds for the object and the object is not a mapping.
    """
    try:
        return obj[index]
    except KeyError:
        return obj["result"][index]


def find_path(name: str, path: str = None) -> str:
    """
    Recursively looks at parent folders starting from the given path until it finds the given name.
    Returns the path as a Path object if found, or None otherwise.
    """
    # If no path is given, use the current working directory
    if path is None:
        path = os.getcwd()

    # Check if the current directory contains the name
    if name in os.listdir(path):
        path_name = os.path.join(path, name)
        print(f"{name} found: {path_name}")
        return path_name

    # Get the parent directory
    parent_directory = os.path.dirname(path)

    # If the parent directory is the same as the current directory, we've reached the root and stop the search
    if parent_directory == path:
        return None

    # Recursively call the function with the parent directory
    return find_path(name, parent_directory)


def add_comfyui_directory_to_sys_path() -> None:
    """
    Add 'ComfyUI' to the sys.path
    """
    comfyui_path = find_path("ComfyUI")
    if comfyui_path is not None and os.path.isdir(comfyui_path):
        sys.path.append(comfyui_path)
        print(f"'{comfyui_path}' added to sys.path")


def add_extra_model_paths() -> None:
    """
    Parse the optional extra_model_paths.yaml file and add the parsed paths to the sys.path.
    """
    from main import load_extra_path_config

    extra_model_paths = find_path("extra_model_paths.yaml")

    if extra_model_paths is not None:
        load_extra_path_config(extra_model_paths)
    else:
        print("Could not find the extra_model_paths config file.")


add_comfyui_directory_to_sys_path()
add_extra_model_paths()
import folder_paths


def import_custom_nodes() -> None:
    """Find all custom nodes in the custom_nodes folder and add those node objects to NODE_CLASS_MAPPINGS

    This function sets up a new asyncio event loop, initializes the PromptServer,
    creates a PromptQueue, and initializes the custom nodes.
    """
    import asyncio
    import execution
    from nodes import init_custom_nodes
    import server

    # Creating a new event loop and setting it as the default loop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    # Creating an instance of PromptServer with the loop
    server_instance = server.PromptServer(loop)
    execution.PromptQueue(server_instance)

    # Initializing custom nodes
    init_custom_nodes()


from nodes import (
    NODE_CLASS_MAPPINGS,
    ImageScale,
    VAELoader,
    ControlNetApplyAdvanced,
    CLIPTextEncode,
    VAEDecode,
    SaveImage,
    KSampler,
    VAEEncode,
    CheckpointLoaderSimple,
)


def process_basic_vid2vid(
        video_path: str = '',
        checkpoint_path: str = '',
        controlnet_path: str = '',
        animatediff_path: str = '',
        vae_path: str = '',
        output_path: str = '',
        prompt: str = '',
        negative_prompt: str = '',
        width: int = 768,
        height: int = 512,
):
    import_custom_nodes()
    folder_paths.output_directory = output_path

    with torch.inference_mode():
        vaeloader = VAELoader()
        vaeloader_2 = vaeloader.load_vae(vae_path=vae_path)

        checkpointloadersimple = CheckpointLoaderSimple()
        checkpointloadersimple_107 = checkpointloadersimple.load_checkpoint(
            ckpt_path=checkpoint_path,
        )

        cliptextencode = CLIPTextEncode()
        cliptextencode_3 = cliptextencode.encode(
            text=prompt, 
            clip=get_value_at_index(checkpointloadersimple_107, 1),
        )

        cliptextencode_6 = cliptextencode.encode(
            text=negative_prompt,
            clip=get_value_at_index(checkpointloadersimple_107, 1),
        )

        vhs_loadvideopath = NODE_CLASS_MAPPINGS["VHS_LoadVideoPath"]()
        vhs_loadvideopath_105 = vhs_loadvideopath.load_video(
            video=video_path,
            force_rate=0,
            force_size="Disabled",
            frame_load_cap=0,
            skip_first_frames=0,
            select_every_nth=25,
        )

        imagescale = ImageScale()
        imagescale_53 = imagescale.upscale(
            upscale_method="nearest-exact",
            width=width,
            height=height,
            crop="disabled",
            image=get_value_at_index(vhs_loadvideopath_105, 0),
        )

        vaeencode = VAEEncode()
        vaeencode_56 = vaeencode.encode(
            pixels=get_value_at_index(imagescale_53, 0),
            vae=get_value_at_index(vaeloader_2, 0),
        )

        controlnetloaderadvanced = NODE_CLASS_MAPPINGS["ControlNetLoaderAdvanced"]()
        controlnetloaderadvanced_70 = controlnetloaderadvanced.load_controlnet(
            controlnet_path=controlnet_path
        )

        ade_animatediffuniformcontextoptions = NODE_CLASS_MAPPINGS[
            "ADE_AnimateDiffUniformContextOptions"
        ]()
        ade_animatediffuniformcontextoptions_94 = (
            ade_animatediffuniformcontextoptions.create_options(
                context_length=16,
                context_stride=1,
                context_overlap=4,
                context_schedule="uniform",
                closed_loop=False,
            )
        )

        ade_animatediffloaderwithcontext = NODE_CLASS_MAPPINGS[
            "ADE_AnimateDiffLoaderWithContext"
        ]()
        lineartpreprocessor = NODE_CLASS_MAPPINGS["LineArtPreprocessor"]()
        controlnetapplyadvanced = ControlNetApplyAdvanced()
        ksampler = KSampler()
        vaedecode = VAEDecode()
        saveimage = SaveImage()
        vhs_videocombine = NODE_CLASS_MAPPINGS["VHS_VideoCombine"]()

        # Process
        ade_animatediffloaderwithcontext_93 = (
            ade_animatediffloaderwithcontext.load_mm_and_inject_params(
                model_path=animatediff_path,
                beta_schedule="sqrt_linear (AnimateDiff)",
                motion_scale=1,
                apply_v2_models_properly=False,
                model=get_value_at_index(checkpointloadersimple_107, 0),
                context_options=get_value_at_index(
                    ade_animatediffuniformcontextoptions_94, 0
                ),
            )
        )

        lineartpreprocessor_71 = lineartpreprocessor.execute(
            resolution=512, image=get_value_at_index(imagescale_53, 0), coarse="disable"
        )

        controlnetapplyadvanced_72 = controlnetapplyadvanced.apply_controlnet(
            strength=0.5,
            start_percent=0,
            end_percent=1,
            positive=get_value_at_index(cliptextencode_3, 0),
            negative=get_value_at_index(cliptextencode_6, 0),
            control_net=get_value_at_index(controlnetloaderadvanced_70, 0),
            image=get_value_at_index(lineartpreprocessor_71, 0),
        )

        ksampler_7 = ksampler.sample(
            seed=random.randint(1, 2**64),
            steps=25,
            cfg=7,
            sampler_name="euler_ancestral",
            scheduler="normal",
            denoise=1,
            model=get_value_at_index(ade_animatediffloaderwithcontext_93, 0),
            positive=get_value_at_index(controlnetapplyadvanced_72, 0),
            negative=get_value_at_index(controlnetapplyadvanced_72, 1),
            latent_image=get_value_at_index(vaeencode_56, 0),
        )

        vaedecode_10 = vaedecode.decode(
            samples=get_value_at_index(ksampler_7, 0),
            vae=get_value_at_index(vaeloader_2, 0),
        )

        saveimage_12 = saveimage.save_images(
            filename_prefix="image",
            images=get_value_at_index(vaedecode_10, 0),
        )

        vhs_videocombine_104 = vhs_videocombine.combine_video(
            frame_rate=8,
            loop_count=0,
            filename_prefix="AnimateDiff",
            format="image/gif",
            pingpong=False,
            save_image=True,
            crf=20,
            save_metadata=True,
            audio_file="",
            images=get_value_at_index(vaedecode_10, 0),
        )
